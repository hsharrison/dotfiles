function psgrep --description 'Search processes'
        if begin; test (count $argv) -eq 1; and contains $argv -h --help; end
                pgrep --help | sed '0,/pgrep/s//psgrep/'
                return
        end

        set pids (pgrep $argv 2>| sed 's/pgrep/psgrep/g' >&2)
        if test (count $pids) -eq 0
                return 1
        end
        
        ps -f $pids
end
