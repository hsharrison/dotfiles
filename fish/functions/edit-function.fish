function edit-function
  set filename "$HOME/.config/fish/functions/$argv.fish"
  if test -n "$DISPLAY"
    command $EDITOR "$filename"
  else
    nano "$filename"
  end
end
