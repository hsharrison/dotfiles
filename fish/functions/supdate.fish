function supdate
    sudo apt-get update
    sudo apt-get -y upgrade
    sudo apt-get dist-upgrade
    sudo apt-get -y autoremove
end
