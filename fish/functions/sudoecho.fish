function sudoecho -d "echo redirect with sudo permission"
    echo $argv[1..-2] | sudo tee -a $argv[-1] > /dev/null
end
