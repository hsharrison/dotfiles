function sshl -a port
    ssh -L $port:localhost:$port $argv[2..-1]
end
