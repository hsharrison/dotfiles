function play
  set pattern $argv[(count $argv)]
  if test (count $argv) -gt 1
    set args $argv[1..(math (count $argv)-1)]
  end
  
  set mkv *$pattern*.mkv
  set srt *$pattern*.srt
  if test -n "$srt"
    set sub_arg -sub $srt[1]
  end

  set all_args -fs -cache 32768 $sub_arg $mkv
  echo mplayer $all_args
  mplayer $all_args
end
