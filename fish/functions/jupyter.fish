function jupyter
  if test (count $argv) -eq 0
    if test -n "$DISPLAY"
      command jupyter qtconsole --style native
    else
      command jupyter console
    end
  else
    command jupyter $argv
  end
end
