function fifa-exports
  set matchday $argv[1]
  set recordings (
    kubectl exec -n $KUBE_NAMESPACE mongo-client -- \
      mongosh mongodb+srv://backend:lGyxVg1AxhumeRbewb6y%2F%2FZnIS7QjsYA@common-pri.z96w6.mongodb.net/backend \
      --quiet \
      --eval "
        process.stdout.write(JSON.stringify(
          db.recording.distinct('_id', {'clientIds': 'la-liga', 'competitionName': 'LaLiga 2023-24', 'matchday': 'Matchday $matchday'})
        ) + '\n');
      " \
    | head -n1 \
    | jq -r .[]
  )
  echo Matchday $matchday recordings: $recordings
  for recording in $recordings
    set exportPath gs://kognia-staging-laliga-share/2023-2024/(string pad -c0 -w2 $matchday)/$recording
    helm upgrade --install \
      -n $KUBE_NAMESPACE \
      fifa-$recording \
      ~/src/optima-workspace/k8s-job-manager/src/k8s_job_manager/charts/fbvars \
      --set google.bucket=kognia-match-data \
      --set fbvars.command=fifa \
      --set fbvars.recording_id=$recording \
      --set fbvars.fifaExportPath=$exportPath \
      --set image.tag=master
  end
end

