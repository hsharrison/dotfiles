# Set environment variables.
set -gx LC_ALL en_US.UTF-8
set -gx LC_CTYPE en_US.UTF-8
set -gx LANG en_US.UTF-8

set -gx EDITOR nano
set -gx VISUAL 'code --wait'
set -gx TERM xterm-256color
set -gx CLICOLOR 1
set -gx CONDA_DIR /opt/conda

set -gx fish_greeting
abbr --add c cd
abbr --add syu sudo yum update
abbr --add syi sudo yum install
abbr --add sdu sudo dnf update
abbr --add sdi sudo dnf install
abbr --add sds sudo dnf search
abbr --add l ls -lAbh
abbr --add ll ls -lbh
abbr --add pi pip install
abbr --add ! sudo
abbr --add c conda
abbr --add ci conda install
abbr --add cu conda update
abbr --add cua conda update --all
abbr --add g git
abbr --add ga git add
abbr --add gam git amend
abbr --add gc git commit
abbr --add gcm git commit --message
abbr --add gcam git commit --all --message
abbr --add g: git status
abbr --add gl git log
abbr --add glc git log ORIG_HEAD.. --stat --no-merges
abbr --add gco git checkout
abbr --add gp git pull
abbr --add gd git diff
abbr --add gdd git diff dev...
abbr --add gfu git fixup
abbr --add gsb git search-branches
abbr --add gcb git checkout-branch
abbr --add gsc git show-commit
abbr --add gcl gcloud
abbr --add gcssh gcloud compute ssh
abbr --add gcscp gcloud compute scp
abbr --add gccgc gcloud container clusters get-credentials
abbr --add k kubectl
abbr --add kn kubectl -n \$KUBE_NAMESPACE
abbr --add kg kubectl get
abbr --add kng kubectl -n \$KUBE_NAMESPACE get
abbr --add kd kubectl describe
abbr --add knd kubectl -n $KUBE_NAMESPACE describe
abbr --add kl kubectl logs 
abbr --add knl kubectl -n \$KUBE_NAMESPACE logs
abbr --add kpf kubectl port-forward
abbr --add knpf kubectl -n \$KUBE_NAMESPACE port-forward
abbr --add ke kubectl exec
abbr --add kne kubectl -n \$KUBE_NAMESPACE exec
abbr --add kx kubectx
abbr --add h helm
abbr --add hn helm -n \$KUBE_NAMESPACE
abbr --add hui helm -n \$KUBE_NAMESPACE upgrade --install
abbr --add hl helm -n \$KUBE_NAMESPACE list

fish_add_path ~/.local/bin
fish_add_path ~/src/google-cloud-sdk/bin
fish_add_path ~/.cabal/bin
fish_add_path ~/conda/bin
