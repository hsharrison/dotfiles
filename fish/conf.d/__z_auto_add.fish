function __z_auto_add --on-variable  PWD  -d 'Set up automatic population of the directory list for z'
	 z --add $PWD
end
