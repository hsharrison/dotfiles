#! /usr/bin/env python3
"""player

Usage:
  player [options] <snippet>

Options:
  -h --help         Show this message
  -d <show-dir>     Directory to start looking for episode [default: .]
  -f <format>       File format [default: .mkv]
  -c <cache>        Cache size [default: 32768]
  -p <position>     Seek to position (seconds or hh:mm:ss)
  --drop            Enable framedropping
  --delay <delay>   A/V delay [default: .22]
  --mplayer <path>  Full path to mplayer [default: mplayer]

"""
import sys
from pathlib import Path
import re
from subprocess import Popen
from docopt import docopt

re_pattern = r'^[sS](?P<season>\d+)[eE](?P<episode>\d+)$'


def get_matches(base_path, snippet, format):
    return list(base_path.glob('*{}*{}'.format(snippet, format)))


def main(argv=None):
    if argv is None:
        argv = sys.argv

    args = docopt(__doc__, argv=argv[1:])
    snippet = args['<snippet>']

    match = re.match(re_pattern, snippet)
    base_dir = Path(args['-d'])
    if match:
        season_path = base_dir / 'Season {}'.format(match.group('season'))
        if season_path.is_dir():
            base_dir = season_path

    mkv_matches = get_matches(base_dir, snippet, args['-f'])
    if len(mkv_matches) > 1:
        raise ValueError('Could not disambiguate snippet {!r}'.format(snippet))
    if not mkv_matches:
        raise ValueError('Could not find file matching snippet {!r}'.format(snippet))
    mkv_match = str(mkv_matches[0])


    all_args = [
        args['--mplayer'],
        '-stop-xscreensaver',
        '-fs',
        '-cache', args['-c'],
        '-sub-fuzziness', '1',
        '-delay', args['--delay'],
    ]
    if args['-p']:
        position = args['-p']
        times = position.split(':')
        if len(times) == 1:
            position = '00:{}:00'.format(position)
        elif len(times) == 2:
            position = '00:{}'.format(position)
        all_args.extend(['-ss', position])
    if args['--drop']:
        all_args.append('-framedrop')
    all_args.append(mkv_match)

    print(' '.join(all_args) + '\n')
    Popen(all_args)

if __name__ == '__main__':
    sys.exit(main())
